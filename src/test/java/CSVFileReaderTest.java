import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class CSVFileReaderTest {
    @Test
    public void should_get_rules_from_CSVfile() throws MyFileReaderException {
        String path = "C:\\Users\\adamk\\IdeaProjects\\CSVReader\\src\\main\\resources\\rules.csv";
        List<Map<String, String>> rulesMap = new CSVFileReader(path).getRules();

        assertEquals("Visa",rulesMap.get(0).get("name"));
        assertEquals("4",rulesMap.get(0).get("prefix"));
        assertEquals("16",rulesMap.get(0).get("length"));

        assertEquals("AmericanExpress",rulesMap.get(7).get("name"));
        assertEquals("37",rulesMap.get(7).get("prefix"));
        assertEquals("15",rulesMap.get(7).get("length"));

    }

    @Test (expected = MyFileReaderException.class)
    public void should_throw_exception() throws MyFileReaderException {
        String path = "C:\\Users\\adamk\\IdeaProjects\\CSVReader\\src\\main\\files\\wrongpath.csv";
        List<Map<String, String>> rulesMap = new CSVFileReader(path).getRules();
    }

}