import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class CSVFileReader implements IFileReader {
    String path;

    public CSVFileReader(String path) {
        this.path = path;
    }

    private static final String CSV_FILE_SEPARATOR = ";";

    @Override
    public List<Map<String, String>> getRules() throws MyFileReaderException {

        List<Map<String, String>> rulesMap = new ArrayList<>();
        try {
            File file = new File(path);
            Scanner scanner = new Scanner(file);
            String[] headLines = scanner.nextLine().split(CSV_FILE_SEPARATOR);

            while (scanner.hasNextLine()) {
                String[] rule = scanner.nextLine().split(CSV_FILE_SEPARATOR);
                Map<String, String> rules = new HashMap<>();

                for (int i = 0; i < headLines.length; i++) {
                    rules.put(headLines[i], rule[i]);
                }
                rulesMap.add(rules);
            }

        } catch (FileNotFoundException e) {
            throw new MyFileReaderException(e.getMessage(), e.getCause());
        }
        return rulesMap;
    }
}
